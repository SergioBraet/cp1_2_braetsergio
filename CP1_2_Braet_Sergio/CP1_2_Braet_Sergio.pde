/* Braet Sergio
 sergio.braet@student.ehb.be
 1BA multimedia en communicatietechnologie
 2016-2017
 
 Minim, BeatDetect
 http://code.compartmental.net/tools/minim/quickstart/
 geraadpleegd op 11/12/2016
 */


//De benodigde bibliotheken en variabelen voor de muziek
import ddf.minim.*;
import ddf.minim.analysis.*;
Minim minim;
AudioPlayer song;

// variabelen voor grootte, gevoeligheid en kleur
int sensitivity= 50;
int size;
int colorValue[]={ round(random(0, 255)), round(random(0, 255)), round(random(0, 255))};
int colorStroke=255;

//instellen grootte, song,...
void setup()
{
  size(1280, 720);
  minim = new Minim(this);
  song = minim.loadFile("song.mp3", 2048);
  song.play();
}
//oproepen functies 
void draw()
{
  //achtergrondkleur en kleur lijnen
  background(colorValue[0], colorValue[1], colorValue[2]);
  stroke(colorStroke);
  //functie 1 nadat er op '1' is gedrukt
  if (key=='1') {
    drawRects();
  }

  //functie 2 nadat er op '2' is gedrukt
  if (key=='2') {
    draw2RowRects();
  }

  //functie 3 nadat er op '3' is gedrukt
  if (key=='3') {
    horizontalRects();
  }

  //functie 4 nadat er op '4' is gedrukt
  if (key=='4') {
    horizontalMultipleRects();
  }

  //functie 5 nadat er op '5' is gedrukt
  if (key=='5') {
    multipleWaves();
  }
}

//eerste functie: Rechthoeken die getekend worden in het midden van het scherm. Ze veranderen van grootte volgens muziek
void drawRects() {
  size=width/4;//variabele voor grootte
  for (int i = 0; i < song.bufferSize() - 1; i++)
  {
    rect(0+(i*1), 0, 1, size+ song.mix.get(i+1)*sensitivity);//rechthoeken die veranderen van grootte
  }
}

//tweede functie: tekent rechthoeken links en recht. Ze veranderen van grootte volgens muziek. Het lijkt alof er 1 algemene balk is in het midden.
void draw2RowRects() {
  size= width/3;//variabele voor grootte
  for (int i = 0; i < song.bufferSize() - 1; i++)
  {
    rect(0, 0+(i*1), size+ song.mix.get(i+1)*sensitivity, 1);//de rechthoeken die veranderen van grootte
    rect(width, 0+(i*1), song.mix.get(i+1)*sensitivity-size, 1);
  }
}

//derde functie: tekent rechtoeken boven en onder die veranderen van grootte volgens de muziek. Hier is er ook het effect van een gemeenschappelijke balk aanwezig.
void horizontalRects() {
  size= height/3;//variabele voor de grootte
  for (int i = 0; i < song.bufferSize() - 1; i++)
  {
    rect(0+(i*1), 0, 1, size+ song.mix.get(i+1)*sensitivity);//de rechthoeken die veranderen van grootte
    rect( 0+(i*1), height, 1, song.mix.get(i+1)*sensitivity-size);
  }
}

//vierde functie: zelfde als derde functie maar breder
void horizontalMultipleRects() {
  size= height/8;//variabele voor grootte
  for (int i = 0; i < song.bufferSize() - 1; i++)
  {
    rect(0+(i*1), 0, 1, size+ song.mix.get(i+1)*sensitivity);//de rechthoeken die veranderen van grootte
    rect( 0+(i*1), height, 1, song.mix.get(i+1)*sensitivity-size);
    rect(0+(i*1), height/2, 1, song.mix.get(i+1)*sensitivity);//de rechthoeken die veranderen van grootte
    rect( 0+(i*1), height/2, 1, song.mix.get(i+1)*sensitivity);
  }
}

//vijfde functie: effect dat je meerdere golven hebt
void multipleWaves() {
  size= height/8;//variabele voor grootte
  for (int i = 0; i < song.bufferSize() - 1; i++)
  {
    line(i, width/2 + song.left.get(i)*50, i+1, 50 + song.left.get(i+1)*50);//de lijnen die veranderen van grootte
  }
}